import { Service, NedbServiceOptions } from 'feathers-nedb';
import { Application } from '../../declarations';

// A type interface for our user (it does not validate any data)
interface RolesData {
  _id?: string;
  key: number;
  name: string;
}

export class Roles extends Service<RolesData> {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<NedbServiceOptions>, app: Application) {
    super(options);
  }

  /** Configuration for swagger interface/documentation */
  docs = {
    description: 'A service to handle User Roles',
    definitions: {
      roles: {
        title: 'Role',
        type: 'object',
        required: ['key', 'name'],
        properties: {
          key: {
            type: 'number',
            description: 'The role numeric id',
          },
          name: {
            type: 'string',
            description: 'The name of the role',
          },
        },
      },
    },

    securities: ['all'],
  };
}
