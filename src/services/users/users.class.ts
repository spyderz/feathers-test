import { Params } from '@feathersjs/feathers';
import crypto from 'crypto';
import { NedbServiceOptions, Service } from 'feathers-nedb';

import { Application } from '../../declarations';

// The Gravatar image service
const gravatarUrl = 'https://s.gravatar.com/avatar';
// The size query, we want a 60px image
const query = 's=60';
const getGravatar = (email: string) => {
  // Gravatar uses MD5 hashes from an email address (all lowercase) to get the image
  const hash = crypto
    .createHash('md5')
    .update(email.toLowerCase())
    .digest('hex');
  // Return the full avatar URL
  return `${gravatarUrl}/${hash}?${query}`;
};

// A type interface for our user (it does not validate any data)
interface UserData {
  _id?: string;
  email: string;
  password: string;
  name?: string;
  role?: number;
  avatar?: string;
  githubId?: string;
}

export class Users extends Service<UserData> {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<NedbServiceOptions>, app: Application) {
    super(options);
  }

  /** Configuration for swagger interface/documentation */
  docs = {
    description: 'A service to handle Users',
    definitions: {
      users: {
        title: 'User',
        type: 'object',
        required: ['email', 'password'],
        properties: {
          email: {
            type: 'string',
            description: 'The users email address',
          },
          name: {
            type: 'string',
            description: 'The users name',
          },
          role: {
            type: 'number',
            description: 'The users role in the application',
          },
          password: {
            type: 'string',
            description: 'The users password',
          },
          githubId: {
            type: 'string',
            description: 'The users github id',
          },
          avatar: {
            type: 'string',
            description: 'Url to the users avatar image',
          },
        },
      },
    },
    securities: ['all']
  }

  create(data: UserData, params?: Params): Promise<UserData | UserData[]> {
    // This is the information we want from the user signup data
    const { email, password, githubId, name, role } = data;
    // Use the existing avatar image or return the Gravatar for the email
    const avatar = data.avatar || getGravatar(email);
    // The complete user
    const userData = {
      email,
      name,
      password,
      role,
      githubId,
      avatar,
    };

    // Call the original `create` method with existing `params` and new data
    return super.create(userData, params);
  }
}
